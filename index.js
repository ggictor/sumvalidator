function twoNumberSum(Array,targetSum){
    for(i=0;i<Array.length;i++){
        for(j=1;j+i<Array.length;j++){
            if((Array[i]+Array[j+i])==targetSum){
                Array_returned=[];
                Array_returned.push(Array[i]);
                Array_returned.push(Array[i+j]);
                return Array_returned;
            }
        }
    }
    return null
}

const array = [3, 5, -4, 8, 11, 1, -1, 6];
const targetSum = 10;
const result = twoNumberSum(array, targetSum);
if (result==null){
    console.log("No hay coincidencias");
}
else{
    console.log("El resultado es:["+result+"]");
}
